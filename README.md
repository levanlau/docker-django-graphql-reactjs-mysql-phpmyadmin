
# README #

This tutorial uses docker(+ django, + mysql, +phpmyadmin) tools to build an app which includes django in role "backend", 
react app in role "frontend", "backend" works with "frontend" throught api "GraphQL". Code in this project is similar to git "https://levanlau@bitbucket.org/levanlau/django-graphql-reactjs-same-hosting.git"

# Step 1: building backend

- docker-compose run web django-admin.py startproject myproject .

- cd backend/myproject > nano settings , add code below to DATABASES = { ... }

  'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'django20',
        'USER': 'root',
		'PASSWORD': 'mypass',
        'HOST': 'db',
        'PORT': 3306,
    }
	
- docker-compose up -d (run background) or: docker-compose up (run foreground to view and bug error)

# Step 2: view backend url

- View http://localhost:8000 (django)

- View http://localhost:8082 (phpmyadmin)

# Step 3: configurate more backend

- Include "graphene_django" in settings.py file

- Create an app named "api" and include "app" in settings.py file as well

- In "api" app, create "schema_v1.py" for the first api version, "schema_v2.py" for 2nd api version, ...etc

- Add code like "schema_v1.py" file

- Add code for url "api/v1" like "project/urls.py" file

- Add "LOGIN_URL = '/admin/login/'" to "settings.py" file to secure url "api/v1" for authencation

- docker exec -it c_web_django bash ("c_web_django" is container name)

- Inside the bash command, type: "python3.5 manage.py migrate" to create database for django, then type: "python3.5 manage.py createsuperuser" to create admin user, the last type: "exit" to exit command window.

- View http://localhost:8000/admin , login with admin user

- View http://localhost:8000/api/v1 to use GraphGL

- View http://localhost:8082, to see data of database "django20"

# Step 4: build frontend

- Alongside "backend" folder, create "frontend" folder and navigate to inside frontend

- Install NodeJs, Npm, or Yarn

- Use Npm(Yarn) to install "create-react-app" package

- Use "create-react-app" to install the first ReactJs app 

- View http://localhost:3000 to see Reactjs app

# Hope userful for you

